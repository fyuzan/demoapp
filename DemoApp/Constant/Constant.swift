//
//  Constant.swift
//  DemoApp
//
//  Created by Fyuzan on 06/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class Constant: NSObject {

    static var SERVICE = "http://dummy.restapiexample.com/api/v1/employees"
    
    static var EmployeeTableCellIdentifire = "EmployeeCell"

}
