//
//  EmployeeViewModel.swift
//  DemoApp
//
//  Created by Fyuzan on 06/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class EmployeeViewModel: NSObject {

    var id : String?
    var employee_name : String?
    var employee_salary : String?
    var employee_age : String?
    var profile_image : String?
    
    init(employee:EmployeeEntity)
    {
        self.id = employee.id
        self.employee_name = employee.employee_name
        self.employee_salary = employee.employee_salary
        self.employee_age = employee.employee_age
        self.profile_image = employee.profile_image
    }
}
