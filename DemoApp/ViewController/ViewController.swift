//
//  ViewController.swift
//  DemoApp
//
//  Created by Fyuzan on 06/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tblEmployeeList: UITableView!
    
    var arrEmployeeData = [EmployeeViewModel]()
    var arrEmployeeDataFromEntity = [EmployeeEntity]()

    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.navigationController?.isNavigationBarHidden = true
        self.tblEmployeeList.delegate = self
        self.tblEmployeeList.dataSource = self
        
        if CheckInternet.Connection() == true
        {
            let empData = self.getDataFromLocal()
            if empData!.count > 0
            {
                self.mapData(empEntityData:empData ?? [])
            }
            else
            {
                self.getData()
            }
        }
        else{
            
            let empData = self.getDataFromLocal()
            if empData!.count > 0
            {
                self.mapData(empEntityData:empData ?? [])
            }
            else
            {
                self.showAlert(self.alertWithTitle("Alert", message: "Not Loaded Employee Data."))
            }
        }
    }
    
    func showAlert(_ alert: UIAlertController) {
        guard self.presentedViewController != nil else {
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    func alertWithTitle(_ title: String, message: String) -> UIAlertController {

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    func getDataFromLocal() -> [EmployeeEntity]?
    {
        let arrEmployee = CoreDataHelper.coreDataSharedInstance.getData()
        return arrEmployee
    }
    
    func getData()
    {
        WebService.sharedInstance.getAllEmployeeData { (employee,error) in
            if(error==nil)
            {
                self.mapData(empEntityData: employee ?? [])
            }
        }
    }
    
    func mapData(empEntityData:[EmployeeEntity])
    {
        self.arrEmployeeDataFromEntity = empEntityData
        self.arrEmployeeData = empEntityData.map({ return EmployeeViewModel(employee: $0)})
                                                       
        DispatchQueue.main.async {
            self.tblEmployeeList.reloadData()
        }
    }
    
    @IBAction func btnSortByNameAction(_ sender: UIButton)
    {
        self.arrEmployeeDataFromEntity = self.arrEmployeeDataFromEntity.sorted { $0.employee_name! < $1.employee_name! }
        self.mapData(empEntityData: self.arrEmployeeDataFromEntity)
    }
        
    @IBAction func btnSortByAgeAction(_ sender: UIButton)
    {
        self.arrEmployeeDataFromEntity = self.arrEmployeeDataFromEntity.sorted { $0.employee_age! < $1.employee_age! }
        self.mapData(empEntityData: self.arrEmployeeDataFromEntity)
    }
}

extension ViewController:UITableViewDataSource,UITableViewDelegate
{
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrEmployeeData.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.EmployeeTableCellIdentifire, for: indexPath)
        
        let empData = self.arrEmployeeData[indexPath.row]
        
        cell.textLabel?.text = empData.employee_name
        cell.detailTextLabel?.text = empData.employee_age
        cell.imageView?.image = UIImage(named: "userIcon")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj : DetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        obj.EmployeeDetail = self.arrEmployeeData[indexPath.row]
        self.navigationController?.pushViewController(obj, animated: true)

    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete
        {
            let afterEmpDeleteData = CoreDataHelper.coreDataSharedInstance.deleteData(index: indexPath.row,empData: self.arrEmployeeDataFromEntity)
            self.mapData(empEntityData: afterEmpDeleteData)
            self.tblEmployeeList.deleteRows(at: [indexPath], with: .automatic)
        }
    }
}
