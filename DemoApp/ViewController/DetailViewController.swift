//
//  DetailViewController.swift
//  DemoApp
//
//  Created by Fyuzan on 06/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var lblEmployeeId: UILabel!
    @IBOutlet weak var lblEmployeeName: UILabel!
    @IBOutlet weak var lblEmployeeAge: UILabel!
    @IBOutlet weak var lblEmployeeSalary: UILabel!
    @IBOutlet weak var imgEmployeeProfileImage: UIImageView!
    
    var EmployeeDetail : EmployeeViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.async {
            self.setEmployeeData()
        }
        // Do any additional setup after loading the view.
    }
    
    func setEmployeeData()
    {
        lblEmployeeId.text = EmployeeDetail?.id
        lblEmployeeName.text = EmployeeDetail?.employee_name
        lblEmployeeAge.text = EmployeeDetail?.employee_age
        lblEmployeeSalary.text = EmployeeDetail?.employee_salary
        imgEmployeeProfileImage.image = UIImage(named: "userIcon")
    }
    
    @IBAction func btnBackAction(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
