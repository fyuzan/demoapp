//
//  EmployeeModel.swift
//  DemoApp
//
//  Created by Fyuzan on 06/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

struct EmployeeResultModel: Codable {
    let status: String
    let data: [EmployeeModel]
}

// MARK: - Datum
struct EmployeeModel: Codable {
    let id, employeeName, employeeSalary, employeeAge: String
    let profileImage: String

    enum CodingKeys: String, CodingKey {
        case id
        case employeeName = "employee_name"
        case employeeSalary = "employee_salary"
        case employeeAge = "employee_age"
        case profileImage = "profile_image"
    }
}

