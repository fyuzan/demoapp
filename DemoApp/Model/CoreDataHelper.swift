//
//  CoreDataHelper.swift
//  DemoApp
//
//  Created by Fyuzan on 07/03/20.
//  Copyright © 2020 Apple. All rights reserved.
// 

import UIKit
import CoreData

class CoreDataHelper: NSObject {

    static var coreDataSharedInstance = CoreDataHelper()
    let contex = AppDelegate.sharedInstanceAppdel.persistentContainer.viewContext

    func saveData(employee:EmployeeModel)
    {
        let employeeDetail = NSEntityDescription.insertNewObject(forEntityName: "EmployeeEntity", into: contex) as! EmployeeEntity
        employeeDetail.id = employee.id
        employeeDetail.employee_name = employee.employeeName
        employeeDetail.employee_age = employee.employeeAge
        employeeDetail.employee_salary = employee.employeeSalary
        employeeDetail.profile_image = employee.profileImage

        do {
            try contex.save()
        } catch {
            print("Data not saved.")
        }
    }
    
    func getData() -> [EmployeeEntity]
    {
        var employeeArray = [EmployeeEntity]()
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "EmployeeEntity")
        do {
            employeeArray = try contex.fetch(fetchRequest) as! [EmployeeEntity]
        } catch  {
            print("can not get data from coredata")
        }
        return employeeArray
    }
    
    func deleteData(index:Int,empData:[EmployeeEntity]) -> [EmployeeEntity]
    {
        var employeeArray = empData
        contex.delete(employeeArray[index])
        employeeArray.remove(at: index)
        
        do {
            try contex.save()
        } catch  {
            print("cannot delete data")
        }
        
        return employeeArray
    }
}
