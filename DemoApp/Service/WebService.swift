//
//  WebService.swift
//  DemoApp
//
//  Created by Fyuzan on 06/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class WebService: NSObject {

    static let sharedInstance = WebService()
    
    func getAllEmployeeData(completion: @escaping([EmployeeEntity]?,Error?) -> ())
    {
        let urlString = Constant.SERVICE
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error
            {
                completion(nil,error)
            }
            else
            {
                guard let data = data else { return }
                do {
                    var arrEmployee = [EmployeeEntity]()

                    let decoder = JSONDecoder()
                    let result = try decoder.decode(EmployeeResultModel.self, from: data)
                    
                    for employee in result.data
                    {
                        let employeeModel = EmployeeModel(
                            id:employee.id,
                            employeeName:employee.employeeName,
                            employeeSalary:employee.employeeSalary,
                            employeeAge:employee.employeeAge,
                            profileImage:employee.profileImage)
                        
                        CoreDataHelper.coreDataSharedInstance.saveData(employee: employeeModel)
                    }
                    arrEmployee = CoreDataHelper.coreDataSharedInstance.getData()
                    print(arrEmployee)
                    completion(arrEmployee,nil)
                } catch let jsonError {
                    print("json error : \(jsonError.localizedDescription)")
                }
            }
        }.resume()
    }
}
